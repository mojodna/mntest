heroku-buildpack-mapnik
-----------------------

Enable the `user-env-compile` Labs feature and set `GITHUB_TOKEN` to provide
npm with appropriate credentials to install the tileserver:

```bash
heroku labs:enable user-env-compile
```
